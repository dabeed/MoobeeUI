//
//  Date.swift
//  Moobee
//
//  Created by Toni García Alhambra on 09/02/2020.
//  Copyright © 2020 Toni García Alhambra. All rights reserved.
//

import Foundation

extension Date {

  init(day: Int, month: Int, year: Int) {
    self.init()

    let gregorianCalendar = NSCalendar(calendarIdentifier: .gregorian)!

    var dateComponents = DateComponents()
    dateComponents.year = year
    dateComponents.month = month
    dateComponents.day = day

    self = gregorianCalendar.date(from: dateComponents)!
  }

  var day: Int {
    let calendar = Calendar.current
    return calendar.component(.day, from: self)
  }

  var month: Int {
    let calendar = Calendar.current
    return calendar.component(.month, from: self)
  }

  var year: Int {
    let calendar = Calendar.current
    return calendar.component(.year, from: self)
  }

  var stringMonth: String {
    let dateFormatter = DateFormatter()
    dateFormatter.setLocalizedDateFormatFromTemplate("MMMM")
    return dateFormatter.string(from: self)
  }

  var monthStringCode: String {
    let dateFormatter = DateFormatter()
    dateFormatter.setLocalizedDateFormatFromTemplate("MMM")
    return dateFormatter.string(from: self)
  }

  func toString(withFormat format: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    return dateFormatter.string(from: self)
  }
}
