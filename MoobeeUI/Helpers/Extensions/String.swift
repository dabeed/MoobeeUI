//
//  String.swift
//  Moobee
//
//  Created by Toni García Alhambra on 06/02/2020.
//  Copyright © 2020 Toni García Alhambra. All rights reserved.
//

import UIKit

extension String {
  // MARK: - Properties
  var convertedToURLRequest: URLRequest? {
    if let URL =  URL(string: self) {
      return URLRequest(url: URL)
    }

    return nil
  }

  var localized: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
  }

  // MARK: - Functions
  func toDate(withFormat format: String)-> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.calendar = Calendar(identifier: .gregorian)
    dateFormatter.dateFormat = format
    let date = dateFormatter.date(from: self)

    return date
  }

  func isValidEmail() -> Bool {
    let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
    return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
  }
}
