//
//  Int.swift
//  Moobee
//
//  Created by Toni García Alhambra on 19/02/2020.
//  Copyright © 2020 Toni García Alhambra. All rights reserved.
//

import Foundation

extension Int {
  func toDate() -> Date {
    return Date(timeIntervalSince1970: TimeInterval(self))
  }
}
