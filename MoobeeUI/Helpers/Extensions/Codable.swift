//
//  Codable.swift
//  Moobee-v2
//
//  Created by David Alhambra on 27/1/21.
//

import Foundation

extension Encodable {
  func toJSON(_ encoder: JSONEncoder = JSONEncoder()) -> String {
    guard let jsonData = try? encoder.encode(self),
          let json = String(data: jsonData, encoding: String.Encoding.utf16)
    else { return "" }
    
    return json
  }

  func transform<T: Decodable>(_ type: T.Type, _ decoder: JSONDecoder = JSONDecoder()) -> T {
    self.toJSON().toObject(T.self)
  }
}

extension String {
  func toObject<T: Decodable>(_ type: T.Type, _ decoder: JSONDecoder = JSONDecoder()) -> T {
    let jsonData = Data(self.utf8)
    return try! decoder.decode(T.self, from: jsonData)
  }
}
