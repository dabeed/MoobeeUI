//
//  MoobeeUIApp.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 12/2/21.
//

import SwiftUI

@main
struct MoobeeUIApp: App {
    let store = AppStore(initialState: .init(search: .init()),
                         reducer: appReducer,
                         environment: AppServices())

    var body: some Scene {
        WindowGroup {
            SearchContainerView()
                .environmentObject(store)
        }
    }
}
