//
//  SearchService.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 12/2/21.
//

import SwiftUI
import Combine

protocol SearchServiceProtocol {
    func search(matching query: String) -> AnyPublisher<[SearchResult], Error>
}

class SearchService: SearchServiceProtocol {
    private let session: URLSession
    private let decoder: JSONDecoder

    init(session: URLSession = .shared, decoder: JSONDecoder = .init()) {
        self.session = session
        self.decoder = decoder
    }

    func search(matching query: String) -> AnyPublisher<[SearchResult], Error> {
        guard
            var urlComponents = URLComponents(string: "\(Endpoint.movieDB.path)/search/multi")
            else { preconditionFailure("Can't create url components...") }

        urlComponents.queryItems = [
            URLQueryItem(name: "api_key", value: APIKEY_theMovieDB),
            URLQueryItem(name: "language", value: languageCode),
            URLQueryItem(name: "query", value: query),
            URLQueryItem(name: "include_adult", value: "false"),
            URLQueryItem(name: "page", value: "1")
        ]

        guard
            let url = urlComponents.url
            else { preconditionFailure("Can't create url from url components...") }

        return session
            .dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: Paginable<SearchResultDTO>.self, decoder: decoder)
            .map { $0.results }
            .map { $0.map({ $0.map() }) }
            .eraseToAnyPublisher()
    }
}
