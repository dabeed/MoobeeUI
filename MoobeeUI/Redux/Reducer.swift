//
//  Reducer.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 12/2/21.
//

import Foundation
import Combine

typealias Reducer<State, Action, Environment> =
    (inout State, Action, Environment) -> AnyPublisher<Action, Never>?

func appReducer(
    state: inout AppState,
    action: AppAction,
    environment: AppServices
) -> AnyPublisher<AppAction, Never> {
    
    switch action {
    case let .search(searchAction):
        return searchReducer(state: &state.search,
                             action: searchAction,
                             environment: environment.searchService)
            .map(AppAction.search)
            .eraseToAnyPublisher()
    }
}

func searchReducer(
    state: inout SearchState,
    action: SearchAction,
    environment: SearchService
) -> AnyPublisher<SearchAction, Never> {
    
    switch action {
    case .setSearchResults(let searchResults):
        state.searchResults = searchResults
        
    case let .search(query):
        return environment
            .search(matching: query)
            .replaceError(with: [])
            .map { SearchAction.setSearchResults($0) }
            .eraseToAnyPublisher()
    }
    
    return Empty().eraseToAnyPublisher()
}
