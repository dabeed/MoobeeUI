//
//  AppAction.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 12/2/21.
//

import Foundation
import Combine

struct AppServices {
    var searchService = SearchService()
}

enum AppAction {
    case search(action: SearchAction)
}

enum SearchAction {
    case setSearchResults(_ searchResults: [SearchResult])
    case search(query: String)
}


typealias AppStore = Store<AppState, AppAction, AppServices>
