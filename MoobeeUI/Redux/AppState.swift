//
//  AppState.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 12/2/21.
//

import Foundation

struct AppState {
    var search: SearchState
}

struct SearchState {
    var searchResults: [SearchResult] = []
}
