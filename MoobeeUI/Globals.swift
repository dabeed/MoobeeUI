//
//  Globals.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 12/2/21.
//

import Foundation

let APIKEY_theMovieDB = "e439a792b107ab5c326080f6535691aa"

enum Endpoint {
  case movieDB
  case moobee

  var path: String {
    switch self {

    case .movieDB:
      return "https://api.themoviedb.org/3"

    case .moobee:
      #if DEVELOP
      return "https://staging-moobee-django.herokuapp.com/api"
      #else
      return "https://api.moobee.app/api"
      #endif
    }
  }
}

var languageCode: String {
  return Locale.current.languageCode ?? "en-US"
}

var countryCode: String {
  return Locale.current.regionCode ?? "US"
}
