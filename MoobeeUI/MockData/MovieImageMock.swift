//
//  MovieImagesMock.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension MovieImage {
    static let mock = MovieImage(filePath: "/nG5zmbVeYlcDhckrPAe06fArywn.jpg")
    
    static let mockArray = [
        mock, mock, mock
    ]
}
