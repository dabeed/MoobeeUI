//
//  MovieCredits.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension PersonCredits {
  static let mock = PersonCredits(cast: PersonCredit.mockArray,
                                  crew: PersonCredit.mockArray)
}
