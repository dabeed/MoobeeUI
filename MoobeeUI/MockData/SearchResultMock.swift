//
//  SearchResultMock.swift
//  MoobeeTests
//
//  Created by David Alhambra on 3/2/21.
//

import Foundation

extension SearchResult {
  static let mock = SearchResult(id: "M-\(152601)", type: .movie(Movie.mock))

  static let mockArray: [SearchResult] = [
    mock, mock, mock
  ]
}
