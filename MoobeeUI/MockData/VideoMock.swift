//
//  VideoMock.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension Video {
  static let mock = Video(id: "5bb9376f0e0a2634ea0170dc",
                          key: "XsQqMwacZQw",
                          site: .youtube,
                          type: .trailer)
  static let mockArray: [Video] = [
    mock, mock, mock
  ]
}
