//
//  Person.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension Person {
  static let mock = Person(id: 2,
                           name: "Joaquin Phoenix",
                           profilePath: "/nXMzvVF6xR3OXOedozfOcoA20xh.jpg",
                           birthday: Date(day: 1, month: 1, year: 1990),
                           biography: "Joaquin Phoenix biography",
                           gender: .male,
                           mainDepartment: .Acting,
                           jobString: "Actor",
                           movieCredits: nil,
                           job: "Actor",
                           knowFor: Movie.mockArray,
                           castCreditsCount: 10,
                           directingCreditsCount: 10,
                           productionCreditsCount: 10)
}
