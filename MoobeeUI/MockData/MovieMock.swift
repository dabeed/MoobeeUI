//
//  MovieMock.swift
//  Moobee-v2Tests
//
//  Created by David Alhambra on 2/2/21.
//

import Foundation

extension Movie {
  static let mock = Movie(id: 152601,
                          title: "Her",
                          originalTitle: "Her",
                          genreIDs: [10749, 878, 18],
                          backdropPath: "/nG5zmbVeYlcDhckrPAe06fArywn.jpg",
                          posterPath: "/yk4J4aewWYNiBhD49WD7UaBBn37.jpg",
                          voteAverage: 7.9000000000000004,
                          popularity: 41.158000000000001,
                          overview: "In the not so distant future, Theodore, a lonely writer purchases a newly developed operating system designed to meet the user's every needs. To Theodore's surprise, a romantic relationship develops between him and his operating system. This unconventional love story blends science fiction and romance in a sweet tale that explores the nature of love and the ways that technology isolates and connects us all.",
                          releaseDate: Date(day: 18, month: 12, year: 2013),
                          runtime: 126,
                          status: .released,
                          tagline: "A Spike Jonze Love Story",
                          similar: nil,
                          belongsToCollection: nil,
                          images: MovieImages.mock,
                          revenue: 47351251,
                          homepage: "http://www.herthemovie.com/",
                          budget: 23000000,
                          genres: Genre.mockArray,
                          videos: Video.mockArray,
                          credits: PersonCredits.mock)
  
  static let emptyMock = Movie(id: 152601,
                               title: "Her",
                               originalTitle: nil,
                               genreIDs: nil,
                               backdropPath: nil,
                               posterPath: nil,
                               voteAverage: nil,
                               popularity: nil,
                               overview: nil,
                               releaseDate: nil,
                               runtime: nil,
                               status: nil,
                               tagline: nil,
                               similar: nil,
                               belongsToCollection: nil,
                               images: nil,
                               revenue: nil,
                               homepage: nil,
                               budget: nil,
                               genres: nil,
                               videos: nil,
                               credits: nil)
  
  static let mockArray: [Movie] = [
    mock, mock, mock
  ]
}
