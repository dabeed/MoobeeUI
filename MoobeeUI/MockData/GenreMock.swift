//
//  GenreMock.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension Genre {
    static let mock = Genre(id: 1, name: "Fantasy")
    static let mockArray: [Genre] = [
        mock, mock, mock
    ]
}
