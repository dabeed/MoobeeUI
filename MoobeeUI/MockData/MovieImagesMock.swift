//
//  MovieImagessMock.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension MovieImages {
  static let mock = MovieImages(backdrops: MovieImage.mockArray,
                                           posters: MovieImage.mockArray)
}
