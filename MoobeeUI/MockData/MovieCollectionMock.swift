//
//  MovieCollection.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension MovieCollection {
    static let mock = MovieCollection(id: 10,
                                      backdropPath: "/d8duYyyC9J5T825Hg7grmaabfxQ.jpg",
                                      posterPath: "/r8Ph5MYXL04Qzu4QBbq2KjqwtkQ.jpg",
                                      name: "Star Wars Collection",
                                      parts: Movie.mockArray)
}
