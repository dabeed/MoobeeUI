//
//  PersonCredits.swift
//  MoobeeTests
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

extension PersonCredit {
  static let mock = PersonCredit(id: 73421,
                                 profilePath: "/nXMzvVF6xR3OXOedozfOcoA20xh.jpg",
                                 name: "Joaquin Phoenix",
                                 department: "Acting",
                                 job: "Actor",
                                 character: "Theodore Twombly")
  
  static let mockArray: [PersonCredit] = [
    mock, mock, mock
  ]
}
