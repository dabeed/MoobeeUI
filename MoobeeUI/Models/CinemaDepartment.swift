//
//  CinemaDepartment.swift
//  Moobee-v2
//
//  Created by David Alhambra on 27/1/21.
//

import Foundation

enum CinemaDepartment: String {
  case None      
  case Acting    
  case Directing 
  case Production
}
