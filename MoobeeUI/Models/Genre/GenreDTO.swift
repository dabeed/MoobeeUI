//
//  GenreDTO.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct GenreDTO: Codable {
  let id: Int
  let name: String

  func map() -> Genre {
    return Genre(id: id,
                 name: name)
  }
}
