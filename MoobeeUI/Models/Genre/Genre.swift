//
//  Genre.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct Genre {
  let id: Int
  let name: String
}
