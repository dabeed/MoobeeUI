//
//  SearchResultDTO.swift
//  Moobee-v2
//
//  Created by David Alhambra on 2/2/21.
//

import Foundation

struct SearchResultDTO: Codable{
  // MARK: - CodingKeys
  private enum CodingKeys: String, CodingKey {
    case mediaType = "media_type"
  }

  // MARK: - Properties
  enum ResultType {
    case movie(MovieDTO)
    case person(PersonDTO)
    case unknown

    var `case`: Case {
      switch self {
      case .movie: return .movie
      case .person: return .person
      case .unknown: return .unknown
      }
    }

    enum Case {
      case movie
      case person
      case unknown
    }
  }

  private let mediaType: String
  let type: ResultType

  // MARK: - Init
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

    if let mediaType = try container.decodeIfPresent(String.self, forKey: .mediaType) {
      self.mediaType = mediaType

      switch mediaType {
      case "movie":
        let movieDTO = try MovieDTO.init(from: decoder)
        self.type = .movie(movieDTO)

      case "person":
        let personDTO = try PersonDTO.init(from: decoder)
        self.type = .person(personDTO)

      default:
        self.type = .unknown
      }
    } else {
      self.mediaType = ""
      self.type = .unknown
    }
  }

  // MARK: - Functions
  func map() -> SearchResult {
    var id: String {
      switch type {
      case .movie(let movie): return "M-\(movie.id)"
      case .person(let person): return "P-\(person.id)"
      case .unknown: return ""
      }
    }

    var resultType: SearchResult.ResultType {
      switch type {
      case .movie(let movie): return .movie(movie.map())
      case .person(let person): return .person(person.map())
      case .unknown: return .unknown
      }
    }

    return SearchResult(id: id, type: resultType)
  }
}
