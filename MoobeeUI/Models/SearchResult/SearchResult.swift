//
//  SearchResult.swift
//  Moobee-v2
//
//  Created by David Alhambra on 27/1/21.
//

import Foundation

struct SearchResult: Hashable, Identifiable {
  // MARK: - Hash
  func hash(into hasher: inout Hasher) {
    hasher.combine(self.id)
  }

  // MARK: - Properties
  enum ResultType: Equatable {
    case movie(Movie)
    case person(Person)
    case unknown
  }

  let id: String
  let type: ResultType
}
