//
//  VideoDTO.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct VideoDTO: Codable {
  let id: String

  let key: String?
  let site: String?
  let type: String?

  func map() -> Video {
    return Video(id: id,
                      key: key,
                      site: Video.Site(rawValue: site ?? ""),
                      type: Video.VideoType(rawValue: type ?? ""))
  }
}
