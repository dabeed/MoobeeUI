//
//  Video.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct Video {
  enum VideoType: String {
    case trailer = "Trailer"
    case teaser = "Teaser"
    case clip = "Clip"
    case featurette = "Featurette"
    case behindTheScenes = "Behind the Scenes"
    case bloopers = "Bloopers"
  }

  enum Site: String {
    case youtube = "YouTube"
    case other   = ""
  }

  let id: String

  let key: String?
  let site: Site?
  let type: VideoType?
}
