//
//  Pagination.swift
//  Moobee-v2
//
//  Created by David Alhambra on 26/1/21.
//

import Foundation

class Pagination<T> {
  typealias Item = T

  var items = [Item]()

  var page: Int = 1
  var itemsReceived: Int { return items.count }

  var hasNextPage:  Bool = true
  var isCalling:    Bool = false
  var numberOfItems: Int = 0

  var isEmpty: Bool {
    itemsReceived == 0
  }

  /// It resets the pagination.
  func resetAndRemove() {
    page   = 1
    hasNextPage  = true
    removeAllItems()
  }

  func resetPage() {
    page   = 1
    hasNextPage  = true
  }

  /// Set the items in the correct position of the array.
  ///
  /// If it's the first page the items will be replaced, else the items will be appended.
  ///
  /// - Parameter items: The items to append or fill the array.
  func setItems(_ items: [Item]) {
    if page == 1 {
      self.items = items
    } else {
      self.items.append(contentsOf: items)
    }

    page += 1
  }

  /// It returns a boolean that means if the table needs to call the service.
  ///
  /// - Parameter currentIndex: The current index of the table.
  /// - Returns: A boolean that that means if the table needs to call the service.
  func needsToReload(currentIndex: Int) -> Bool {
    return numberOfItems - currentIndex < 5
  }

  /// It return the item at given position.
  ///
  /// - Parameter index: The position of the array.
  /// - Returns: The item.
  func getItemAt(index: Int) -> Item? {
    return items.count > index ? items[index] : nil
  }

  /// It returns all the items.
  ///
  /// - Returns: All the items in the array.
  func getitems() -> [Item] {
    return items
  }

  /// Removes the items in the items array.
  ///
  /// - Parameters:
  ///   - indexes: An array containing the indexes of which items must be deleted.
  func removeItems(at indexes: [Int]) {
    indexes.forEach { (index) in
      items.remove(at: index)
    }
  }

  /// Remove all items in the items array.
  func removeAllItems() {
    items.removeAll()
  }
}

class Paginable<T: Codable>: Codable {
  var total_results: Int = 0
  var total_pages: Int = 0
  var page: Int = 0

  var results = [T]()
}
