//
//  PersonCredit.swift
//  Moobee-v2
//
//  Created by David Alhambra on 27/1/21.
//

import Foundation

struct PersonCredit {
  let id: Int

  let profilePath: String?
  let name: String?
  let department: String?
  let job: String?
  let character: String?
  
  var posterPathURLw500: String? {
    guard let imagePath = profilePath else { return nil }
    return "https://image.tmdb.org/t/p/w500/\(imagePath)"
  }

  var posterPathURLOriginalSize: String? {
    guard let imagePath = profilePath else { return nil }
    return "https://image.tmdb.org/t/p/original/\(imagePath)"
  }
}
