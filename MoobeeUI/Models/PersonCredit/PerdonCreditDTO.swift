//
//  PerdonCreditDTO.swift
//  Moobee-v2
//
//  Created by David Alhambra on 2/2/21.
//

import Foundation

struct PersonCreditDTO: Codable {
  let id: Int

  let profile_path: String?
  let name: String?
  let department: String?
  let job: String?
  let character: String?

  func map() -> PersonCredit {
    return PersonCredit(id: id,
                        profilePath: profile_path,
                        name: name,
                        department: department,
                        job: job,
                        character: character)
  }
}
