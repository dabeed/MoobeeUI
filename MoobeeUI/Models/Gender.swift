//
//  Gender.swift
//  Moobee-v2
//
//  Created by David Alhambra on 27/1/21.
//

import UIKit

enum Gender {
  case male
  case female
  case other
}
