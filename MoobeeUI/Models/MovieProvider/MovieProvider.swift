//
//  MovieProvider.swift
//  Moobee
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

struct MovieProvider {
  let currency: String
  let quality: VideoQuality
  let type: ProviderMethod
  let url: String
  let provider: Provider?
}
