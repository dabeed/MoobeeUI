//
//  MovieProviderDTO.swift
//  Moobee
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

struct MovieProviderDTO: Codable {
  let currency: String
  let quality: String
  let type: String
  let url: String
  let provider: Int
  
  func map() -> MovieProvider {
    return MovieProvider(currency: currency,
                         quality: VideoQuality(rawValue: quality) ?? .sd,
                         type: ProviderMethod(rawValue: type) ?? .buy,
                         url: url,
                         provider: Provider.shared.first(where: { $0.id == provider }))
  }
}
