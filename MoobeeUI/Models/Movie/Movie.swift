//
//  Movie.swift
//  Moobee-v2
//
//  Created by David Alhambra on 2/2/21.
//

import Foundation

struct Movie: Equatable, Hashable {
  static func == (lhs: Movie, rhs: Movie) -> Bool {
    lhs.id == rhs.id
  }

  func hash(into hasher: inout Hasher) {
    hasher.combine(self.id)
  }

  enum Status: String {
    case rumored = "Rumored"
    case planned = "Planned"
    case inProduction = "In Production"
    case postProduction = "Post Production"
    case released = "Released"
    case canceled = "Canceled"
  }

  // MARK: - Init
  let id: Int

  let title: String
  let originalTitle: String?
  let genreIDs: [Int]?
  let backdropPath: String?
  var posterPath: String?
  let voteAverage: Double?
  let popularity: Double?
  let overview: String?
  let releaseDate: Date?

  let runtime: Int?
  let status: Status?
  let tagline: String?
  let similar: [Movie]?
  var belongsToCollection: MovieCollection?
  let images: MovieImages?
  let revenue: Int?
  let homepage: String?
  let budget: Int?
  let genres: [Genre]?
  let videos: [Video]?
  let credits: PersonCredits?

  var posterPathURLw500: String? {
    guard let imagePath = posterPath else { return nil }
    return "https://image.tmdb.org/t/p/w500/\(imagePath)"
  }

  var posterPathURLOriginalSize: String? {
    guard let imagePath = posterPath else { return nil }
    return "https://image.tmdb.org/t/p/original/\(imagePath)"
  }

  var youtubeTrailers: [Video]? {
    return videos?.filter({ $0.site == .youtube && $0.type == .trailer })
  }

  var mainTrailerKey: String? {
    if let trailers = youtubeTrailers, trailers.count > 0, let trailerKey = trailers[0].key {
      return trailerKey
    }

    return nil
  }

  var directors: [PersonCredit]? {
    return credits?.crew?.filter({ $0.job == "Director" })
  }

  // MARK: - Functions
  func getGenresString() -> String? {
    return genres?.compactMap({$0.name}).prefix(3).joined(separator: ", ")
  }

  func getTimeAndYearString() -> String? {
    var movieData = [String]()

    if let releaseYear = releaseDate?.year { movieData.append("\(releaseYear)") }
    if let runtime = runtime, runtime > 0 { movieData.append("\(runtime)min") }

    return movieData.compactMap({$0}).joined(separator: " · ")
  }
}
