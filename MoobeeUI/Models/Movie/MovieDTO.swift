//
//  Movie.swift
//  Moobee-v2
//
//  Created by David Alhambra on 26/1/21.
//

import Foundation

struct MovieDTO: Codable {
  let id: Int

  let title: String
  let original_title: String?
  let genre_ids: [Int]?
  let backdrop_path: String?
  var poster_path: String?
  let vote_average: Double?
  let popularity: Double?
  let overview: String?
  let release_date: String?

  let runtime: Int?
  let status: String?
  let tagline: String?
  let similar: Paginable<MovieDTO>?
  let belongs_to_collection: MovieCollectionDTO?
  let images: MovieImagesDTO?
  let revenue: Int?
  let homepage: String?
  let budget: Int?
  let genres: [GenreDTO]?
  let videos: Results<VideoDTO>?
  let credits: PersonCreditsDTO?

  func map() -> Movie {
    return Movie(id: id,
                 title: title,
                 originalTitle: original_title,
                 genreIDs: genre_ids,
                 backdropPath: backdrop_path,
                 posterPath: poster_path,
                 voteAverage: vote_average,
                 popularity: popularity,
                 overview: overview,
                 releaseDate: release_date?.toDate(withFormat: "yyyy-MM-dd"),
                 runtime: runtime,
                 status: Movie.Status(rawValue: status ?? ""),
                 tagline: tagline,
                 similar: similar?.results.compactMap({ $0.map() }),
                 belongsToCollection: belongs_to_collection?.map(),
                 images: images?.map(),
                 revenue: revenue,
                 homepage: homepage,
                 budget: budget,
                 genres: genres?.compactMap({ $0.map() }),
                 videos: videos?.results.compactMap({ $0.map() }),
                 credits: credits?.map())
  }
}
