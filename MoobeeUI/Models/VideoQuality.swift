//
//  VideoQuality.swift
//  Moobee
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

enum VideoQuality: String {
  case sd = "sd"
  case hd = "hd"
  case qhd = "qhd"

  var title: String {
    switch self {
    case .sd:  return "SD"
    case .hd:  return "HD"
    case .qhd: return "4K"
    }
  }
}
