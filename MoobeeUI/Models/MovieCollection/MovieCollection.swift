//
//  MovieCollection.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct MovieCollection {
  let id: Int

  let backdropPath: String?
  let posterPath: String?
  let name: String?

  let parts: [Movie]?
  
  internal init(id: Int, backdropPath: String?, posterPath: String?, name: String?, parts: [Movie]?) {
    self.id = id
    self.backdropPath = backdropPath
    self.posterPath = posterPath
    self.name = name
    self.parts = parts?.sorted(by: { $0.releaseDate?.compare($1.releaseDate ?? Date.distantFuture) == .orderedAscending })
  }
}
