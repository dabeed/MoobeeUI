//
//  MovieCollectionDTO.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct MovieCollectionDTO: Codable {
  let id: Int

  let backdrop_path: String?
  let poster_path: String?
  let name: String?

  let parts: [MovieDTO]?

  func map() -> MovieCollection {
    return MovieCollection(id: id,
                           backdropPath: backdrop_path,
                           posterPath: poster_path,
                           name: name,
                           parts: parts?.compactMap({ $0.map() }))
  }
}
