//
//  PersonCreditsDTO.swift
//  Moobee-v2
//
//  Created by David Alhambra on 2/2/21.
//

import Foundation

struct PersonCreditsDTO: Codable {
  var cast: [PersonCreditDTO]? // TODO: - Esto es un array de peliculas, subnormal
  var crew: [PersonCreditDTO]?

  func map() -> PersonCredits {
    let castMap = cast?.compactMap({ $0.map() })
    let crewMap = crew?.compactMap({ $0.map() })

    return PersonCredits(cast: castMap,
                  crew: crewMap,
                  directedMovies: castMap?.filter({ $0.department == "Directing" }),
                  producedMovies: castMap?.filter({ $0.department == "Producer" }))
  }
}
