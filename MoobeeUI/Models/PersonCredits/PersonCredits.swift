//
//  PersonCredits.swift
//  Moobee-v2
//
//  Created by David Alhambra on 27/1/21.
//

import Foundation

struct PersonCredits {
  var cast: [PersonCredit]?
  var crew: [PersonCredit]?

  var directedMovies: [PersonCredit]?
  var producedMovies: [PersonCredit]?
}
