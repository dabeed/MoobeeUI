//
//  MovieImage.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct MovieImage {
  var filePath: String

  var w500Path: String {
    return "https://image.tmdb.org/t/p/w500/\(filePath)"
  }

  var originalSizePath: String {
    return "https://image.tmdb.org/t/p/original/\(filePath)"
  }
}
