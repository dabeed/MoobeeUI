//
//  MovieImageDTO.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct MovieImageDTO: Codable {
  var file_path: String

  func map() -> MovieImage {
    return MovieImage(filePath: file_path)
  }
}
