//
//  ProviderDTO.swift
//  Moobee
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

struct ProviderDTO: Codable {
  let id: Int
  let name: String
  let key: String
  let priority: Int
  let icon: String
  
  func map() -> Provider {
    return Provider(id: id,
                    name: name,
                    key: key,
                    priority: priority,
                    iconURL: URL(string: icon))
  }
}
