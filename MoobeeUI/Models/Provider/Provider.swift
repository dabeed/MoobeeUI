//
//  Provider.swift
//  Moobee
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

struct Provider {
  static var shared = [Provider]()
  
  let id: Int
  let name: String
  let key: String
  let priority: Int
  let iconURL: URL?
}
