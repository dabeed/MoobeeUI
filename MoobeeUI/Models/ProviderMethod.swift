//
//  ProviderMethod.swift
//  Moobee
//
//  Created by David García Alhambra on 9/2/21.
//

import Foundation

enum ProviderMethod: String {
  case buy = "flatrate"
  case rent = "rent"
  case streaming = "streaming"

  var title: String {
    switch self {
    case .buy: return String.Common.Buy.localized
    case .rent: return String.Common.Rent.localized
    case .streaming: return String.Common.Streaming.localized
    }
  }
}
