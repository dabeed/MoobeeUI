//
//  Results.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

class Results<T: Codable>: Codable {
  var results = [T]()
}
