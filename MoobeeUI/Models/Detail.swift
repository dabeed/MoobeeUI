//
//  Detail.swift
//  Moobee
//
//  Created by Toni García Alhambra on 30/05/2020.
//  Copyright © 2020 Toni García Alhambra. All rights reserved.
//

import Foundation

enum Detail: Hashable {
  case movie(Movie)
  case person(Person)
}
