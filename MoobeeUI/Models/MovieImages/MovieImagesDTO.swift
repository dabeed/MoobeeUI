//
//  MovieImagesDTO.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct MovieImagesDTO: Codable {
  var backdrops: [MovieImageDTO]?
  var posters: [MovieImageDTO]?

  func map() -> MovieImages {
    return MovieImages(backdrops: backdrops?.compactMap({ $0.map() }),
                       posters: posters?.compactMap({ $0.map() }))
  }
}
