//
//  MovieImages.swift
//  Moobee
//
//  Created by David Alhambra on 4/2/21.
//

import Foundation

struct MovieImages {
  var backdrops: [MovieImage]?
  var posters: [MovieImage]?
}
