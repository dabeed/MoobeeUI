//
//  PersonDTO.swift
//  Moobee-v2
//
//  Created by David Alhambra on 2/2/21.
//

import Foundation

struct PersonDTO: Codable {
  let id: Int
  var name: String

  var profile_path: String?
  var department: String?
  var job: String?
  var birthday: String?
  var biography: String?

  private var known_for_department: String?
  private var gender: Int?

  var movie_credits: PersonCreditsDTO?
  var know_for: [MovieDTO]?

  func map() -> Person {
    var genderMap: Gender {
      switch gender {
      case 0: return .male
      case 1: return .female
      default: return .other
      }
    }

    var mainDepartmentDTO: CinemaDepartment {
      switch known_for_department {
      case "Acting": return .Acting
      case "Directing": return .Directing
      case "Production": return .Production
      default: return .None
      }
    }

    var jobString: String {
      switch mainDepartmentDTO {
      case .Acting:
        switch genderMap {
        case .male, .other: return String.Common.Actor.localized
        case .female: return String.Common.Actress.localized
        }

      case .Directing:
        switch genderMap {
        case .male, .other: return String.Common.DirectorM.localized
        case .female: return String.Common.DirectorF.localized
        }

      case .Production:
        switch genderMap {
        case .male, .other: return String.Common.ProducerM.localized
        case .female: return String.Common.ProducerF.localized
        }

      default: return String.Common.Person.localized
      }
    }

    let credits = movie_credits.map({ $0.map() })

    return Person(id: id,
                  name: name,
                  profilePath: profile_path,
                  birthday: birthday?.toDate(withFormat: "yyyy-MM-dd"),
                  biography: biography,
                  gender: genderMap,
                  mainDepartment: mainDepartmentDTO,
                  jobString: jobString,
                  movieCredits: movie_credits?.map(),
                  job: job,
                  knowFor: know_for?.compactMap({ $0.map() }) ?? [],
                  castCreditsCount: credits?.cast?.count ?? 0,
                  directingCreditsCount: credits?.directedMovies?.count ?? 0,
                  productionCreditsCount: credits?.producedMovies?.count ?? 0)
  }
}
