//
//  Perdon.swift
//  Moobee-v2
//
//  Created by David Alhambra on 27/1/21.
//

import Foundation

struct Person: Equatable, Hashable {
  static func == (lhs: Person, rhs: Person) -> Bool {
    lhs.id == rhs.id
  }

  func hash(into hasher: inout Hasher) {
    hasher.combine(self.id)
  }

  let id: Int
  let name: String
  let profilePath: String?
  let birthday: Date?
  let biography: String?
  let gender: Gender
  let mainDepartment: CinemaDepartment
  let jobString: String
  let movieCredits: PersonCredits?
  let job: String?
  let knowFor: [Movie]
  let castCreditsCount: Int
  let directingCreditsCount: Int
  let productionCreditsCount: Int

  var posterPathURLw500: String? {
    guard let imagePath = profilePath else { return nil }
    return "https://image.tmdb.org/t/p/w500/\(imagePath)"
  }

  var posterPathURLOriginalSize: String? {
    guard let imagePath = profilePath else { return nil }
    return "https://image.tmdb.org/t/p/original/\(imagePath)"
  }
}
