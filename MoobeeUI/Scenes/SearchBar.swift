//
//  SearchBar.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 12/2/21.
//

import SwiftUI

struct SearchResultRow: View {
    let searchResult: SearchResult
    
    var body: some View {
        HStack(alignment: .top) {
            VStack(alignment: .leading) {
                Text("movie.title")
                    .font(.headline)
                Text("movie.overview")
                    .font(.subheadline)
            }
        }
    }
}

struct SearchContainerView: View {
    @EnvironmentObject var store: AppStore
    @State private var query: String = ""

    var body: some View {
        SearchView(
            query: $query,
            searchResults: store.state.search.searchResults,
            onCommit: fetch
        ).onAppear(perform: fetch)
    }

    private func fetch() {
        store.dispatch(.search(action: .search(query: query)))
    }
}


struct SearchView: View {
    @Binding var query: String
    let searchResults: [SearchResult]
    let onCommit: () -> Void

    var body: some View {
        NavigationView {
            List {
                TextField("Type something", text: $query, onCommit: onCommit)

                if searchResults.isEmpty {
                    Text("Loading...")
                } else {
                    ForEach(searchResults) { searchResult in
                        SearchResultRow(searchResult: searchResult)
                    }
                }
            }.navigationBarTitle(Text("Search"))
        }
    }
}
