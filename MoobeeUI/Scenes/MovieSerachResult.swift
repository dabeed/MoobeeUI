//
//  MovieSerachResult.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 13/2/21.
//

import SwiftUI
import Combine

struct MovieSerachResult: View {
  let movie: Movie
  
  var body: some View {
    HStack {
      ImageView(withURL: movie.posterPathURLw500 ?? "")
        .frame(width: 100.0, height: 144.0)
        .cornerRadius(10)
      
      VStack(
        alignment: .leading,
        spacing: 2
      ) {
        Tag(text: String.Common.Movie.localized, color: Color("GreenEmerald"))
        
        HStack(alignment: .center) {
          Text(movie.title)
            .font(.title)
            .fontWeight(.bold)
          
          if let releaseDate = movie.releaseDate?.toString(withFormat: "dd/MM/yyyy") {
            Text("(\(releaseDate))")
              .font(.headline)
              .foregroundColor(Color.gray)
          }
        }
        
        if let overview = movie.overview {
          Text(overview)
            .font(.body)
            .lineLimit(3)
        }
      }
      .frame(height: 150.0)
    }
  }
}

struct MovieSerachResult_Previews: PreviewProvider {
  static var previews: some View {
    MovieSerachResult(movie: Movie.mock)
      .previewDevice("iPhone 12 mini")
  }
}
