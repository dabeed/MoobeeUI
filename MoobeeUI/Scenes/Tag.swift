//
//  Tag.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 13/2/21.
//

import SwiftUI

struct Tag: View {
  let text: String
  let color: Color
  
  var body: some View {
    Text(text.uppercased())
      .font(.headline)
      .padding(.horizontal, 6.0)
      .padding(.vertical, 3.0)
      .background(color)
      .cornerRadius(5.0)
  }
}

struct Tag_Previews: PreviewProvider {
  static var previews: some View {
    Tag(text: "movie", color: .orange)
  }
}
