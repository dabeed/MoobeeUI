//
//  ImageView.swift
//  MoobeeUI
//
//  Created by David García Alhambra on 13/2/21.
//

import SwiftUI

struct ImageView: View {
  @ObservedObject var imageLoader: ImageLoader
  @State var image: UIImage = UIImage()
  
  init(withURL url: String) {
    imageLoader = ImageLoader(urlString:url)
  }
  
  var body: some View {
    Image(uiImage: image)
      .resizable()
      .aspectRatio(contentMode: .fit)
      .onReceive(imageLoader.didChange) { data in
        self.image = UIImage(data: data) ?? UIImage()
      }
  }
}
