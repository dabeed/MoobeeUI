import UIKit
protocol Localizable: CustomStringConvertible {
    var rawValue: String { get }
}

extension Localizable {
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }

    var uppercased: String {
        return self.localized.uppercased()
    }

    var description: String {
        return self.localized
    }

    func localized(with: CVarArg...) -> String {
        let text = String(format: self.localized, arguments: with)
        return text
    }
}
extension String {
   enum App: String, Localizable { 
       case Name = "App_Name"
   }
   enum Common: String, Localizable { 
       case Cancel = "Common_Cancel"
       case Close = "Common_Close"
       case Search = "Common_Search"
       case Save = "Common_Save"
       case Done = "Common_Done"
       case SaveImage = "Common_SaveImage"
       case Delete = "Common_Delete"
       case Edit = "Common_Edit"
       case Add = "Common_Add"
       case Yes = "Common_Yes"
       case No = "Common_No"
       case Email = "Common_Email"
       case Username = "Common_Username"
       case Password = "Common_Password"
       case Available = "Common_Available"
       case Unavailable = "Common_Unavailable"
       case Buy = "Common_Buy"
       case Rent = "Common_Rent"
       case Streaming = "Common_Streaming"
       case Movie = "Common_Movie"
       case Movies = "Common_Movies"
       case Person = "Common_Person"
       case Persons = "Common_Persons"
       case Actor = "Common_Actor"
       case Actress = "Common_Actress"
       case DirectorM = "Common_DirectorM"
       case DirectorF = "Common_DirectorF"
       case ProducerM = "Common_ProducerM"
       case ProducerF = "Common_ProducerF"
       case ViewMore = "Common_ViewMore"
   }
   enum Alert: String, Localizable { 
       case LogOut = "Alert_LogOut"
   }
   enum Error: String, Localizable { 
       case EmailFormat = "Error_EmailFormat"
       case Characters = "Error_Characters"
       case EmptyField = "Error_EmptyField"
       case InvalidCredetials = "Error_InvalidCredetials"
       case AlreadyRegistered = "Error_AlreadyRegistered"
       case AddMovie = "Error_AddMovie"
       case NoImages = "Error_NoImages"
       case NoProviders = "Error_NoProviders"
       case TryAgainLater = "Error_TryAgainLater"
   }
   enum Startup: String, Localizable { 
       case Title = "Startup_Title"
       case Subtitle = "Startup_Subtitle"
       case Login = "Startup_Login"
       case CreateAccount = "Startup_CreateAccount"
   }
   enum Section: String, Localizable { 
       case Discover = "Section_Discover"
       case Lists = "Section_Lists"
   }
   enum Discover: String, Localizable { 
       case Title = "Discover_Title"
       case SearchPlaceholder = "Discover_SearchPlaceholder"
       case SectionPopular = "Discover_SectionPopular"
       case SectionBest = "Discover_SectionBest"
       case SectionTheaters = "Discover_SectionTheaters"
       case SectionNext = "Discover_SectionNext"
   }
   enum MovieDetail: String, Localizable { 
       case Seen = "MovieDetail_Seen"
       case NotSeen = "MovieDetail_NotSeen"
       case Cast = "MovieDetail_Cast"
       case Images = "MovieDetail_Images"
       case Media = "MovieDetail_Media"
       case Trailer = "MovieDetail_Trailer"
       case SimilarMovies = "MovieDetail_SimilarMovies"
       case Collection = "MovieDetail_Collection"
       case SelectDirector = "MovieDetail_SelectDirector"
       case WhereToWatch = "MovieDetail_WhereToWatch"
   }
   enum PersonDetail: String, Localizable { 
       case AsCast = "PersonDetail_AsCast"
       case AsDirector = "PersonDetail_AsDirector"
       case AsProducer = "PersonDetail_AsProducer"
   }
   enum Lists: String, Localizable { 
       case Title = "Lists_Title"
       case MyLists = "Lists_MyLists"
       case MoobeeLists = "Lists_MoobeeLists"
       case CreateListAdvice = "Lists_CreateListAdvice"
       case EmptyMyLists = "Lists_EmptyMyLists"
   }
   enum ListDetail: String, Localizable { 
       case NewList = "ListDetail_NewList"
       case NewListPlaceholder = "ListDetail_NewListPlaceholder"
       case EditTitle = "ListDetail_EditTitle"
       case ChangeName = "ListDetail_ChangeName"
       case EditImage = "ListDetail_EditImage"
       case RemoveMovies = "ListDetail_RemoveMovies"
   }
   enum CreateList: String, Localizable { 
       case ListNamePlaceholder = "CreateList_ListNamePlaceholder"
       case NameTutorial = "CreateList_NameTutorial"
       case MoviesTutorial = "CreateList_MoviesTutorial"
       case NumbersOfMoviesSelected = "CreateList_NumbersOfMoviesSelected"
   }
   enum ListMovieSelector: String, Localizable { 
       case SearchPlaceholder = "ListMovieSelector_SearchPlaceholder"
   }
   enum MovieBackdropSelector: String, Localizable { 
       case SelectImage = "MovieBackdropSelector_SelectImage"
       case UseRandom = "MovieBackdropSelector_UseRandom"
   }
   enum Providers: String, Localizable { 
       case EmptyProviders = "Providers_EmptyProviders"
   }
}
